#!/usr/bin/env node

const
  COMMANDER = require('commander'),

  INDEX_BUILDER_DEVELOPMENT_CONFIG = require('../config/indexBuilder.development'),
  INDEX_BUILDER_PRODUCTION_CONFIG = require('../config/indexBuilder.production'),
  MANIFEST_BUILDER_DEVELOPMENT_CONFIG = require('../config/manifestBuilder.development'),
  MANIFEST_BUILDER_PRODUCTION_CONFIG = require('../config/manifestBuilder.production'),
  CSS_BUNDLER_DEVELOPMENT_CONFIG = require('../config/cssBundler.development'),
  CSS_BUNDLER_PRODUCTION_CONFIG = require('../config/cssBundler.production'),
  JS_BUNDLER_DEVELOPMENT_CONFIG = require('../config/jsBundler.development'),
  JS_BUNDLER_PRODUCTION_CONFIG = require('../config/jsBundler.production'),
  IMAGE_BUILDER_CONFIG = require('../config/imageBuilder'),
  FILE_COPIER_CONFIG = require('../config/fileCopier'),

  JOBS_PROMISE = require('../lib/JobsPromise'),

  INDEX_BUILDER = require('../lib/indexBuilder'),
  MANIFEST_BUILDER = require('../lib/manifestBuilder'),
  CSS_BUNDLER = require('../lib/cssBundler'),
  JS_BUNDLER = require('../lib/jsBundler'),
  IMAGE_BUILDER = require('../lib/imageBuilder'),
  FILE_COPIER = require('../lib/fileCopier');

let
  indexBuilderConfig,
  manifestBuilderConfig,
  cssBundlerConfig,
  jsBundlerConfig;

COMMANDER
  .option('-d --development', 'Build a development everything bagel.')
  .option('-p --production', 'Build a production everything bagel.')
  .option('-w --watch', 'Watch files.')
  .parse(process.argv);

main();

function main() {
  if ((process.argv.slice(2).length === 0) ||
      (!COMMANDER.development && !COMMANDER.production && COMMANDER.watch)) {

    COMMANDER.outputHelp();
    process.exit();
  }

  setConfigs();

  prebuild().then(
    () => build(),
    onrejected => process.exit(onrejected.errno || 1)
  );
}

function setConfigs() {
  if (COMMANDER.development) {

    indexBuilderConfig = INDEX_BUILDER_DEVELOPMENT_CONFIG;
    manifestBuilderConfig = MANIFEST_BUILDER_DEVELOPMENT_CONFIG;
    cssBundlerConfig = CSS_BUNDLER_DEVELOPMENT_CONFIG;
    jsBundlerConfig = JS_BUNDLER_DEVELOPMENT_CONFIG;

  } else if (COMMANDER.production) {

    indexBuilderConfig = INDEX_BUILDER_PRODUCTION_CONFIG;
    manifestBuilderConfig = MANIFEST_BUILDER_PRODUCTION_CONFIG;
    cssBundlerConfig = CSS_BUNDLER_PRODUCTION_CONFIG;
    jsBundlerConfig = JS_BUNDLER_PRODUCTION_CONFIG;

  }
}

function prebuild() {
  return new Promise((resolve, reject) => {

    const JOBS = [
      'indexBuilder',
      'manifestBuilder',
      'cssBundler',
      'jsBundler',
    ];

    const _JOBS_PROMISE = new JOBS_PROMISE(JOBS, resolve, reject);

    INDEX_BUILDER.prebuild(indexBuilderConfig).then(
      () => _JOBS_PROMISE.resolve(JOBS[0]),
      onrejected => _JOBS_PROMISE.reject(JOBS[0], onrejected)
    );

    MANIFEST_BUILDER.prebuild(manifestBuilderConfig).then(
      () => _JOBS_PROMISE.resolve(JOBS[1]),
      onrejected => _JOBS_PROMISE.reject(JOBS[1], onrejected)
    );

    CSS_BUNDLER.prebuild(cssBundlerConfig).then(
      () => _JOBS_PROMISE.resolve(JOBS[2]),
      onrejected => _JOBS_PROMISE.reject(JOBS[2], onrejected)
    );

    JS_BUNDLER.prebuild(jsBundlerConfig).then(
      () => _JOBS_PROMISE.resolve(JOBS[3]),
      onrejected => _JOBS_PROMISE.reject(JOBS[3], onrejected)
    );
  });
}

function build() {
  if (!COMMANDER.watch) {

    new Promise((resolve, reject) => {

      const JOBS = [
        'indexBuilder',
        'manifestBuilder',
        'cssBundler',
        'jsBundler',
        'imageBuilder',
        'fileCopier',
      ];

      const _JOBS_PROMISE = new JOBS_PROMISE(JOBS, resolve, reject);

      INDEX_BUILDER.build(indexBuilderConfig).then(
        () => _JOBS_PROMISE.resolve(JOBS[0]),
        onrejected => _JOBS_PROMISE.reject(JOBS[0], onrejected)
      );

      MANIFEST_BUILDER.build(manifestBuilderConfig).then(
        () => _JOBS_PROMISE.resolve(JOBS[1]),
        onrejected => _JOBS_PROMISE.reject(JOBS[1], onrejected)
      );

      CSS_BUNDLER.build(cssBundlerConfig).then(
        () => _JOBS_PROMISE.resolve(JOBS[2]),
        onrejected => _JOBS_PROMISE.reject(JOBS[2], onrejected)
      );

      JS_BUNDLER.build(jsBundlerConfig).then(
        () => _JOBS_PROMISE.resolve(JOBS[3]),
        onrejected => _JOBS_PROMISE.reject(JOBS[3], onrejected)
      );

      IMAGE_BUILDER.build(IMAGE_BUILDER_CONFIG).then(
        () => _JOBS_PROMISE.resolve(JOBS[4]),
        onrejected => _JOBS_PROMISE.reject(JOBS[4], onrejected)
      );

      FILE_COPIER.copy(FILE_COPIER_CONFIG).then(
        () => _JOBS_PROMISE.resolve(JOBS[5]),
        onrejected => _JOBS_PROMISE.reject(JOBS[5], onrejected)
      );
    }).then(
      () => process.exit(),
      onrejected => process.exit(onrejected.errno || 1)
    );

  } else {

    INDEX_BUILDER.watch(indexBuilderConfig);
    MANIFEST_BUILDER.watch(manifestBuilderConfig);
    CSS_BUNDLER.watch(cssBundlerConfig);
    JS_BUNDLER.watch(jsBundlerConfig);
    IMAGE_BUILDER.watch(IMAGE_BUILDER_CONFIG);
    FILE_COPIER.watch(FILE_COPIER_CONFIG);

  }
}
