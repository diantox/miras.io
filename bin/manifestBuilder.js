#!/usr/bin/env node

const
  COMMANDER = require('commander'),
  MANIFEST_BUILDER_DEVELOPMENT_CONFIG = require('../config/manifestBuilder.development.config'),
  MANIFEST_BUILDER_PRODUCTION_CONFIG = require('../config/manifestBuilder.production.config'),
  MANIFEST_BUILDER = require('../lib/manifestBuilder');

let config;

COMMANDER
  .option('-d --development', 'Build a development manifest.')
  .option('-p --production', 'Build a production manifest.')
  .option('-w --watch', 'Watch files.')
  .parse(process.argv);

main();

async function main() {
  if ((process.argv.slice(2).length === 0) ||
      (!COMMANDER.development && !COMMANDER.production && COMMANDER.watch)) {

    COMMANDER.outputHelp();
    process.exit();
  }

  if (COMMANDER.development) config = MANIFEST_BUILDER_DEVELOPMENT_CONFIG;
  else if (COMMANDER.production) config = MANIFEST_BUILDER_PRODUCTION_CONFIG;

  await MANIFEST_BUILDER.prebuild(config);

  if (!COMMANDER.watch) {

    MANIFEST_BUILDER.build(config).then(
      () => process.exit(),
      onrejected => process.exit(onrejected.errno || 1)
    );

  } else MANIFEST_BUILDER.watch(config);
}
