#!/usr/bin/env node

const
  COMMANDER = require('commander'),
  INDEX_BUILDER_DEVELOPMENT_CONFIG = require('../config/indexBuilder.development'),
  INDEX_BUILDER_PRODUCTION_CONFIG = require('../config/indexBuilder.production'),
  INDEX_BUILDER = require('../lib/indexBuilder');

let config;

COMMANDER
  .option('-d --development', 'Build a development index.')
  .option('-p --production', 'Build a production index.')
  .option('-w --watch', 'Watch files.')
  .parse(process.argv);

main();

async function main() {
  if ((process.argv.slice(2).length === 0) ||
      (!COMMANDER.development && !COMMANDER.production && COMMANDER.watch)) {

    COMMANDER.outputHelp();
    process.exit();
  }

  if (COMMANDER.development) config = INDEX_BUILDER_DEVELOPMENT_CONFIG;
  else if (COMMANDER.production) config = INDEX_BUILDER_PRODUCTION_CONFIG;

  await INDEX_BUILDER.prebuild(config);

  if (!COMMANDER.watch) {

    INDEX_BUILDER.build(config).then(
      () => process.exit(),
      onrejected => process.exit(onrejected.errno || 1)
    );

  } else INDEX_BUILDER.watch(config);
}
