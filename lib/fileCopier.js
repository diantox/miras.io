/**
 * File Copier Configuration
 * @typedef {Object} FileCopierConfig
 * @property {Object.<string, string>} copyFileOpts Copy file options
 * @property {ChokidarOpts} chokidarOpts Chokidar options
 */

/**
 * Chokidar Options
 * @typedef {Object} ChokidarOpts
 * @property {ChokidarWatchOpts} watchOpts Chokidar watch options
 */

/**
 * Chokidar Watch Options
 * See the [official documentation]{@link https://github.com/paulmillr/chokidar/tree/3409db854565caeb17f87d1e5492a507c2a9103e#api}.
 * @typedef {Object} ChokidarWatchOpts
 * @property {boolean} [persistent]
 * @property {*} [ignored]
 * @property {boolean} [ignoreInitial]
 * @property {boolean} [followSymlinks]
 * @property {string} [cwd]
 * @property {boolean} [disableGlobbing]
 * @property {boolean} [usePolling]
 * @property {boolean} [useFsEvents]
 * @property {boolean} [alwaysStat]
 * @property {number} [depth]
 * @property {number} [interval]
 * @property {number} [binaryInterval]
 * @property {boolean} [ignorePermissionErrors]
 * @property {boolean|number} [atomic]
 * @property {ChokidarAwaitWriteFinishOpts|boolean} [awaitWriteFinish]
 */

/**
 * Chokidar Await Write Finish Options
 * @typedef {Object} ChokidarAwaitWriteFinishOpts
 * @property {number} [stabilityThreshold]
 * @property {number} [pollInterval]
 */

const
  PATH = require('path'),
  CHOKIDAR = require('chokidar'),
  JOBS_PROMISE = require('./JobsPromise'),
  MKDIR = require('./mkdir'),
  COPY_FILE = require('./copyFile');

const LOG_MODULE = 'File Copier';

/**
 * Copy files.
 * @param {FileCopierConfig} config The file copier configuration.
 * @returns {Promise} A Promise with an Error on rejection.
 */
function copy(config) {

  /* Return a Promise so we can use JavaScript's async & await features. */
  return new Promise((resolve, reject) => {

    /* Create an empty list of jobs. */
    const JOBS = [];

    /* Populate the list of jobs. */
    for (const SRC_FILE in config.copyFileOpts) {
      JOBS.push(`${SRC_FILE} => ${config.copyFileOpts[SRC_FILE]}`);
    }

    /* Create a JobsPromise. */
    const _JOBS_PROMISE = new JOBS_PROMISE(JOBS, resolve, reject);

    for (const SRC_FILE in config.copyFileOpts) {

      /* Make the directory where the file will be copied to. */
      const DIRNAME = PATH.dirname(config.copyFileOpts[SRC_FILE]);
      MKDIR(LOG_MODULE, DIRNAME).then(
        () => {

          /* If the directory was made without Error, copy the file. */
          COPY_FILE(LOG_MODULE, SRC_FILE, config.copyFileOpts[SRC_FILE]).then(

            /* If the file was copied without Error, resolve the JobsPromise. */
            () => _JOBS_PROMISE.resolve(`${SRC_FILE} => ${config.copyFileOpts[SRC_FILE]}`),

            /* If the file couldn't be copied,
             * reject the JobsPromise with the Error. */
            copyFile_onrejected => _JOBS_PROMISE.reject(`${SRC_FILE} => ${config.copyFileOpts[SRC_FILE]}`, copyFile_onrejected)
          );
        },

        /* If the directory couldn't be made,
         * reject the JobsPromise with the Error. */
        mkdir_onrejected => _JOBS_PROMISE.reject(`${SRC_FILE} => ${config.copyFileOpts[SRC_FILE]}`, mkdir_onrejected)
      );
    }
  });
}

function configAddPaths(config, configCpy, paths) {
  if (typeof paths === 'string') {
    paths = [paths];
  }

  paths.forEach(path => {
    configCpy.copyFileOpts[path] = config.copyFileOpts[path];
  });

  return configCpy;
}

function configChangePaths(config, paths) {
  if (typeof paths === 'string') {
    paths = [paths];
  }

  const CONFIG_CPY = {
    copyFileOpts: {},
    chokidarOpts: config.chokidarOpts,
  };

  paths.forEach(path => {
    CONFIG_CPY.copyFileOpts[path] = config.copyFileOpts[path];
  });

  return CONFIG_CPY;
}

function configUnlinkPaths(config, configCpy, paths) {
  if (typeof paths === 'string') {
    paths = [paths];
  }

  paths.forEach(path => {
    delete configCpy.copyFileOpts[path];
  });

  return configCpy;
}

/**
 * Gracefully copy files (handle Promise resolution & rejection internally).
 * @param {FileCopierConfig} config The file copier configuration.
 */
function gracefullyCopy(config) {
  copy(config).then(
    () => {},
    () => {}
  );
}

/**
 * Watch & copy files.
 * @param {FileCopierConfig} config The file copier configuration.
 */
function watch(config) {
  const CONFIG_CPY = {
    copyFileOpts: {},
    chokidarOpts: config.chokidarOpts,
  };

  const SRC_FILES = Object.keys(config.copyFileOpts);

  CHOKIDAR.watch(SRC_FILES, config.chokidarOpts.watchOpts)
    .on('add', paths => gracefullyCopy(configAddPaths(config, CONFIG_CPY, paths)))
    .on('change', paths => gracefullyCopy(configChangePaths(config, paths)))
    .on('unlink', paths => gracefullyCopy(configUnlinkPaths(config, CONFIG_CPY, paths)));
}

exports.copy = copy;
exports.watch = watch;
