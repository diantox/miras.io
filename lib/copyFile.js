const
  FS = require('fs'),
  LOG = require('./log');

/**
 * Copy a file.
 * @param {string} logModule The log's module.
 * @param {string|Buffer|URL|number} srcPath The source file's path.
 * @param {string|Buffer|URL|number} destPath The destination file's path.
 * @returns {Promise} A Promise with a string on resolution & an Error on rejection.
 */
function copyFile(logModule, srcPath, destPath) {

  /* Return a Promise so we can use JavaScript's async & await features. */
  return new Promise((resolve, reject) => {

    /* Log what we're about to do. */
    LOG.logWarning(logModule, `Copying ${srcPath} => ${destPath}`);
    FS.copyFile(srcPath, destPath, err => {
      if (err) {

        /* Log what we couldn't do, the Error's message,
         * & reject the Promise with the Error. */
        LOG.logError(logModule, `Couldn't copy ${srcPath} => ${destPath}`);
        LOG.logError(err.message);
        reject(err);

      } else {

        /* Log what we did & resolve the Promise with the data. */
        LOG.logSuccess(logModule, `Copied ${srcPath} => ${destPath}`);
        resolve();

      }
    });
  });
}

module.exports = copyFile;
