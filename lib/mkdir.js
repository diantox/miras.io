const
  MKDIRP = require('mkdirp'),
  LOG = require('./log');

/**
 * Make a directory.
 * @param {string} logModule The log's module.
 * @param {string} path The directory's path.
 * @returns {Promise} A Promise with an Error on rejection.
 */
function mkdir(logModule, path) {

  /* Return a Promise so we can use JavaScript's async & await features. */
  return new Promise((resolve, reject) => {

    /* Log what we're about to do. */
    LOG.logWarning(logModule, `Making ${path}`);
    MKDIRP(path, err => {
      if (err) {

        /* Log what we couldn't do, the Error's message,
         * & reject the Promise with the Error. */
        LOG.logError(logModule, `Couldn't make ${path}`);
        LOG.logError(logModule, err.message);
        reject(err);

      } else {

        /* Log what we did & resolve the Promise. */
        LOG.logSuccess(logModule, `Made ${path}`);
        resolve();

      }
    });
  });
}

module.exports = mkdir;
