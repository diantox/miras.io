/**
 * CSS Bundler Configuration
 * @typedef {Object} CssBundlerConfig
 * @property {NodeSassOpts} nodeSassOpts node-sass options
 * @property {ChokidarOpts} chokidarOpts Chokidar options
 */

/**
 * node-sass Options
 * See the [official documentation]{@link https://github.com/sass/node-sass/blob/8319be23bf9b56ef64c9341bff8c1f6f5f093060/README.md#options}.
 * @typedef {Object} NodeSassOpts
 * @property {string} [file]
 * @property {string} [data]
 * @property {*} [importer]
 * @property {*} [functions]
 * @property {Array.<string>} [includePaths]
 * @property {boolean} [indentedSyntax]
 * @property {string} [indentType]
 * @property {number} [indentWidth]
 * @property {string} [linefeed]
 * @property {boolean} [omitSourceMapUrl]
 * @property {string} [outFile]
 * @property {'compact'|'compressed'|'expanded'|'nested'} [outputStyle]
 * @property {number} [precision]
 * @property {boolean} [sourceComments]
 * @property {boolean|string} [sourceMap]
 * @property {boolean} [sourceMapContents]
 * @property {boolean} [sourceMapEmbed]
 * @property {string} [sourceMapRoot]
 */

/**
 * Chokidar Options
 * @typedef {Object} ChokidarOpts
 * @property {string|Array.<string>} paths Paths to be watched.
 * @property {ChokidarWatchOpts} watchOpts Chokidar watch options
 */

/**
 * Chokidar Watch Options
 * See the [official documentation]{@link https://github.com/paulmillr/chokidar/tree/3409db854565caeb17f87d1e5492a507c2a9103e#api}.
 * @typedef {Object} ChokidarWatchOpts
 * @property {boolean} [persistent]
 * @property {*} [ignored]
 * @property {boolean} [ignoreInitial]
 * @property {boolean} [followSymlinks]
 * @property {string} [cwd]
 * @property {boolean} [disableGlobbing]
 * @property {boolean} [usePolling]
 * @property {boolean} [useFsEvents]
 * @property {boolean} [alwaysStat]
 * @property {number} [depth]
 * @property {number} [interval]
 * @property {number} [binaryInterval]
 * @property {boolean} [ignorePermissionErrors]
 * @property {boolean|number} [atomic]
 * @property {ChokidarAwaitWriteFinishOpts|boolean} [awaitWriteFinish]
 */

/**
 * Chokidar Await Write Finish Options
 * @typedef {Object} ChokidarAwaitWriteFinishOpts
 * @property {number} [stabilityThreshold]
 * @property {number} [pollInterval]
 */

const
  PATH = require('path'),
  NODE_SASS = require('node-sass'),
  CHOKIDAR = require('chokidar'),
  LOG = require('./log'),
  MKDIR = require('./mkdir'),
  WRITE_FILE = require('./writeFile');

const LOG_MODULE = 'CSS Bundler';

/**
 * Make the directory where the CSS bundle will be written to.
 * @param {CssBundlerConfig} config The CSS bundler configuration
 * @returns {Promise} A Promise with an Error on rejection.
 */
function prebuild(config) {
  const DIRNAME = PATH.dirname(config.nodeSassOpts.outFile);
  return MKDIR(LOG_MODULE, DIRNAME);
}

/**
 * Build the CSS bundle.
 * @param {CssBundlerConfig} config The CSS bundler configuration
 * @returns {Promise} A Promise with an Error on rejection.
 */
function build(config) {

  /* Return a Promise so we can use JavaScript's async & await features. */
  return new Promise((resolve, reject) => {

    /* Log what we're about to do. */
    LOG.logWarning(LOG_MODULE, `Building ${config.nodeSassOpts.file}`);
    NODE_SASS.render(config.nodeSassOpts, (render_err, result) => {
      if (render_err) {

        /* Log what we couldn't do, the Error's formatted message,
         * & reject the Promise with the Error. */
        LOG.logError(LOG_MODULE, `Couldn't build ${config.nodeSassOpts.file}`);
        LOG.logError(LOG_MODULE, render_err.formatted);
        reject(render_err);

      } else {

        /* Log what we did & write the CSS bundle. */
        LOG.logSuccess(LOG_MODULE, `Built ${config.nodeSassOpts.file}`);
        WRITE_FILE(LOG_MODULE, config.nodeSassOpts.outFile, result.css).then(
          () => {

            /* If the CSS bundle was written without Error,
             * get the source map configuration & convert it to a string if it's a truthy boolean. */
            let sourceMap = config.nodeSassOpts.sourceMap;
            if (typeof sourceMap === 'boolean' && sourceMap) {
              sourceMap = `${config.nodeSassOpts.outFile}.map`;
            }

            if (typeof sourceMap === 'string' && sourceMap) {

              /* If the source map configuration is a truthy string,
               * write the CSS bundle source map to it. */
              WRITE_FILE(LOG_MODULE, sourceMap, result.map).then(

                /* If the CSS bundle source map was written without Error,
                 * resolve the Promise. */
                () => resolve(),

                /* If the CSS bundle source map couldn't be written,
                 * reject the Promise with the Error. */
                _rejected => reject(_rejected)
              );

            } else {

              /* If the source map configuration wasn't a truthy string,
               * resolve the Promise. */
              resolve();

            }
          },

          /* If the CSS bundle couldn't be written,
           * reject the Promise with the Error. */
          rejected => reject(rejected)
        );
      }
    });
  });
}

/**
 * Gracefully build the CSS bundle (handle Promise resolution & rejection internally).
 * @param {CssBundlerConfig} config The CSS bundler configuration
 */
function gracefullyBuild(config) {
  build(config).then(
    () => {},
    () => {}
  );
}

/**
 * Watch & gracefully build the CSS bundle.
 * @param {CssBundlerConfig} config The CSS bundler configuration
 */
function watch(config) {
  CHOKIDAR.watch(config.chokidarOpts.paths, config.chokidarOpts.watchOpts)
    .on('add', () => gracefullyBuild(config))
    .on('change', () => gracefullyBuild(config))
    .on('unlink', () => gracefullyBuild(config));
}

exports.prebuild = prebuild;
exports.build = build;
exports.watch = watch;
