const
  FS = require('fs'),
  LOG = require('./log');

/**
 * Write a file.
 * @param {string} logModule The log's module.
 * @param {string|Buffer|URL|number} path The file's path.
 * @param {string|Buffer|TypedArray|DataView} data The file's data.
 * @returns {Promise} A Promise with an Error on rejection.
 */
function writeFile(logModule, path, data) {

  /* Return a Promise so we can use JavaScript's async & await features. */
  return new Promise((resolve, reject) => {

    /* Log what we're about to do. */
    LOG.logWarning(logModule, `Writing ${path}`);
    FS.writeFile(path, data, { mode: 0o644 }, err => {
      if (err) {

        /* Log what we couldn't do, the Error's message,
         * & reject the Promise with the Error. */
        LOG.logError(logModule, `Couldn't write ${path}`);
        LOG.logError(err.message);
        reject(err);

      } else {

        /* Log what we did & resolve the Promise. */
        LOG.logSuccess(logModule, `Wrote ${path}`);
        resolve();

      }
    });
  });
}

module.exports = writeFile;
