const
  RESOLVE = require('rollup-plugin-node-resolve'),
  BABEL = require('rollup-plugin-babel'),
  TERSER = require('rollup-plugin-terser'),
  DIRS = require('./dirs');

const
  EXTENSIONS = [ '.js', '.jsx', '.ts', '.tsx', ],
  INCLUDE = DIRS.srcDir + '/**/*.(js|jsx|ts|tsx)',
  EXCLUDE = 'node_modules';

module.exports = {
  inputOpts: {
    input: DIRS.srcDir + '/main.ts',
    plugins: [
      RESOLVE({
        extensions: EXTENSIONS,
      }),
      BABEL({
        extensions: EXTENSIONS,
        include: INCLUDE,
        exclude: EXCLUDE,
      }),
      TERSER.terser({
        // TODO: configure Terser
      }),
    ],
    external: [
      'typed.js',
    ],
  },
  outputOpts: {
    file: DIRS.buildDir + '/bundle.js',
    format: 'iife',
    name: 'powerSwordBundle',
    globals: {
      'typed.js': 'Typed',
    },
  },
  watchOpts: {
    include: INCLUDE,
    exclude: EXCLUDE,
    clearScreen: false,
  },
};
