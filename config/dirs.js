const
  SRC_DIR = 'src',
  BUILD_DIR = 'build',
  ASSETS_DIR = 'assets',
  IMAGES_DIR = ASSETS_DIR + '/images',
  FONTS_DIR = ASSETS_DIR + '/fonts';

module.exports = {
  srcDir: SRC_DIR,
  buildDir: BUILD_DIR,
  assetsDir: ASSETS_DIR,
  imagesDir: IMAGES_DIR,
  fontsDir: FONTS_DIR,
};
