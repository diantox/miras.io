const DIRS = require('./dirs');

const IMAGE_BUILDER_CONFIG = {
  sharpOpts: {},
  chokidarOpts: {
    watchOpts: {},
  },
};

IMAGE_BUILDER_CONFIG.sharpOpts[`${DIRS.srcDir}/${DIRS.imagesDir}/icon.svg`] = [
  {
    buildFile: `${DIRS.buildDir}/${DIRS.imagesDir}/icon-mdpi.png`,
    width: 48,
    height: 48,
  },
  {
    buildFile: `${DIRS.buildDir}/${DIRS.imagesDir}/icon-hdpi.png`,
    width: 72,
    height: 72,
  },
  {
    buildFile: `${DIRS.buildDir}/${DIRS.imagesDir}/icon-xhdpi.png`,
    width: 96,
    height: 96,
  },
  {
    buildFile: `${DIRS.buildDir}/${DIRS.imagesDir}/icon-xxhdpi.png`,
    width: 144,
    height: 144,
  },
  {
    buildFile: `${DIRS.buildDir}/${DIRS.imagesDir}/icon-xxxhdpi.png`,
    width: 192,
    height: 192,
  },  {
    buildFile: `${DIRS.buildDir}/${DIRS.imagesDir}/icon-64dp.png`,
    width: 256,
    height: 256,
  },
];

module.exports = IMAGE_BUILDER_CONFIG;
