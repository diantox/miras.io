const DIRS = require('./dirs');

const FILE_COPIER_CONFIG = {
  copyFileOpts: {},
  chokidarOpts: {
    watchOpts: {},
  },
};

FILE_COPIER_CONFIG.copyFileOpts['node_modules/typed.js/lib/typed.min.js'] = `${DIRS.buildDir}/typed.min.js`;
FILE_COPIER_CONFIG.copyFileOpts[`${DIRS.srcDir}/${DIRS.fontsDir}/mplus-1p-light.woff`] = `${DIRS.buildDir}/${DIRS.fontsDir}/mplus-1p-light.woff`;
FILE_COPIER_CONFIG.copyFileOpts[`${DIRS.srcDir}/${DIRS.fontsDir}/mplus-1p-regular.woff`] = `${DIRS.buildDir}/${DIRS.fontsDir}/mplus-1p-regular.woff`;

module.exports = FILE_COPIER_CONFIG;
