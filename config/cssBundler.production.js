const
  NODE_SASS_MAGIC_IMPORTER = require('node-sass-magic-importer'),
  DIRS = require('./dirs');

module.exports = {
  nodeSassOpts: {
    file: DIRS.srcDir + '/main.scss',
    importer: NODE_SASS_MAGIC_IMPORTER(),
    outFile: DIRS.buildDir + '/bundle.css',
    outputStyle: 'compressed',
  },
  chokidarOpts: {
    paths: DIRS.srcDir + '/**/*.scss',
    watchOpts: {},
  },
};
