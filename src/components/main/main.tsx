import m from "mithril";
import { JsxElement } from "typescript";
import { MeComponent } from "../me/me";

class MainComponent {

  public readonly version = m.version;
  public readonly type = "MainComponent";

  public view(): JsxElement {
    return <main class="mainComponent">
      <MeComponent></MeComponent>
    </main>;
  }
}

export { MainComponent };
