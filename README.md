# Power Sword
A web app template using Sass, Rollup, Babel, TypeScript, and Mithril.js.

## Pipelines
Rather than using webpack to build everything through a single pipeline, Power Sword uses different pipelines to build different files.

### Index Builder
**npm/Yarn Script**: `indexBuilder`  
**Binary**: `bin/indexBuilder.js`  
**Development Configuration**: `config/indexBuilder.development.js`  
**Production Configuration**: `config/indexBuilder.production.js`  
**Library**: `lib/indexBuilder.js`  
**Description**: Builds `src/index.html` => `build/index.html` with [HTMLMinifier](https://github.com/kangax/html-minifier).

### Manifest Builder
**npm/Yarn Script**: `manifestBuilder`  
**Binary**: `bin/manifestBuilder.js`  
**Development Configuration**: `config/manifestBuilder.development.js`  
**Production Configuration**: `config/manifestBuilder.production.js`  
**Library**: `lib/manifestBuilder.js`  
**Description**: Builds `src/manifest.json` => `build/manifest.json`.

### CSS Bundler
**npm/Yarn Script**: `cssBundler`  
**Binary**: `bin/cssBundler.js`  
**Development Configuration**: `config/cssBundler.development.js`  
**Production Configuration**: `config/cssBundler.production.js`  
**Library**: `lib/cssBundler.js`  
**Description**: Builds `src/main.scss` => `build/main.css` with [node-sass](https://github.com/sass/node-sass).

### JS Bundler
**npm/Yarn Script**: `jsBundler`  
**Binary**: `bin/jsBundler.js`  
**Development Configuration**: `config/jsBundler.development.js`  
**Production Configuration**: `config/jsBundler.production.js`  
**Library**: `lib/jsBundler.js`  
**Description**: Builds `src/main.ts` => `build/main.js` with [Rollup](https://github.com/rollup/rollup), [Babel](https://github.com/babel/babel), and [TypeScript](https://github.com/Microsoft/TypeScript).

### Image Builder
**npm/Yarn Script**: `imageBuilder`  
**Binary**: `bin/imageBuilder.js`  
**Configuration**: `config/imageBuilder.js`  
**Library**: `lib/imageBuilder.js`  
**Description**: Builds images with [sharp](https://github.com/lovell/sharp).

### File Copier
**npm/Yarn Script**: `fileCopier`  
**Binary**: `bin/fileCopier.js`  
**Configuration**: `config/fileCopier.js`  
**Library**: `lib/fileCopier.js`  
**Description**: Copies files.

### Everything Bagel
**npm/Yarn Script**: `everythingBagel`  
**Binary**: `bin/everythingBagel.js`  
**Description**: Runs all of the pipelines in parallel.

## Extra Scripts
### ESLint
**npm/Yarn Script**: `eslint`  
**Configuration**: `.eslintrc.json`  
**Description**: Lints `bin/**/*.js`, `config/**/*.js`, and `lib/**/*.js`.

### Index Linter
**npm/Yarn Script**: `indexLinter`  
**Description**: Lints `src/index.html` with [HTMLLint](https://github.com/kangax/html-lint).

### JS Linter
**npm/Yarn Script**: `jsLinter`  
**Configuration**: `tslint.json`  
**Description**: Lints `src/**/*(.ts|.tsx)` with [TSLint](https://github.com/palantir/tslint).

### HTTP Server
**npm/Yarn Script**: `http-server`  
**Description**: Serves `build` at `http://127.0.0.1:8080` with [http-server](https://github.com/indexzero/http-server).

### Clean
**npm/Yarn Script** `clean`  
**Description**: Removes `build` with [rimraf](https://github.com/isaacs/rimraf).
